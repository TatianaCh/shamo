
$(document).ready(function(){
    //const PHONE_REGEX = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;

    $('.js-main-menu').click(function(event){
        event.preventDefault();
        event.stopPropagation();
        $('.navbar').addClass('dropdown-view');
        
        $(document).one('click',function(event){
            $('.navbar').removeClass('dropdown-view');
        });
    });


    $(".main-menu").on("click","li", function (event) {
        event.preventDefault();
        if (!$(this).find('a').length) {
            return;
        }
        var id  = $(this).find('a').attr('href');
        if (id) {
            var top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 500);
        }
    });

    $('.toggle-modal').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();

        var modalId = '';
        if ($(this).data()['modal']) {
            modalId = $(this).data()['modal'];
            $(modalId).fadeIn();
        }
    });

    $('.js-toggle-picture').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();

        $('.picture-lightbox').html($(this).html());
    });

    $('.js-modal-close').on('click', function(event) {
        closeModal();
    });

    $('.modal').on('click', function(event) {
        if (event.target.classList.contains('modal')) {
            closeModal();
        }       
    });

    $('.js-send-assignment').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();

        var form = $(this).closest('form');
        var confidence = true;
        var inputs = form.find('.form-input');

        form.find('.error-field').removeClass('error-field');

        for (var i = 0; i < inputs.length; i++) {
            if ($(inputs[i]).val().length === 0) {
                $(inputs[i]).addClass('error-field');
            }
        }
        /*var phoneInput = form.find("[name='phone']");
        
        if (phoneInput.length) {
            if (!PHONE_REGEX.test(phoneInput.val())) {
                phoneInput.addClass('error-field');
            }
        }*/

        if (form.find('.js-confidence-checkbox').length) {
            confidence = form.find('.js-confidence-checkbox').is(":checked");
            if (confidence === false) {
                form.find('.js-confidence-checkbox').addClass('error-field');
            }
        }

        if (form.find('.error-field').length === 0 && confidence) {
            //$('#assingment-sended-modal').fadeIn();
            sendForm(event.target.form);
        }
    });

    setTimeout(function() {
        $('body').removeClass('loading');
    }, 1000);
});

function sendForm(form) {
    var formId = '', posName = '', posPhone = '';

    try {
        formId = form.id;
        posName = $("#" + formId + " [name='name']").val();
        posPhone = $("#" + formId + " [name='phone']").val();
    } catch (e) {
        console.log('error while getting info', e);
        return;
    }

    $.ajax({
        type: "POST",
        url: "./application.php",
        data: {"name": posName, "phone": posPhone},
        cache: false,
        success: function(response){
            if(response == 1){
                $('#assingment-sended-success-modal').fadeIn();
            } else {
                console.log('success bad response', arguments);
                $('#assingment-sended-fail-modal').fadeIn();
            }
        },
        error: function(response) {
            console.log('error', arguments);
            $('#assingment-sended-fail-modal').fadeIn();
        }
    });
    return false;
}

function closeModal() {
    if ($('.confidence-lightbox:visible').length) {
        $('.confidence-lightbox').fadeOut();
        return;
    }
    if ($('#assingment-sended-fail-modal:visible').length) {
        $('#assingment-sended-fail-modal').fadeOut();
        return;
    }
    $('.modal').fadeOut();
    $('.modal .error-field').removeClass('error-field');
    $('.modal .form-input').val('');
    $('.modal input[type="checkbox"]').prop('checked', false);
}